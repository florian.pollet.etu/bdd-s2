\echo Exercice 1: Comprendre les données
\echo Q2.5
\echo (a)

SELECT COUNT(*) FROM import;

\echo (b)
SELECT COUNT(DISTINCT n3) FROM import;

\echo (c)
SELECT COUNT(n110) FROM import WHERE n9='Lille';

\echo (d)
SELECT COUNT(n110) FROM import WHERE n3='0597215X';

\echo (e)
SELECT n110 FROM import WHERE n9='Villeneuve-d''Ascq' AND n10='BUT - Informatique';


\echo Exercice 3 : Requêtage

\echo Q1
\echo Nous obtenons la réponse suivante avec LIMIT 10:
SELECT n56, n57 + n58 + n59 AS ResultByMe FROM import limit 10;

\echo Q2
\echo Si aucune ligne avec des données s'affiche c'est qu'il n'y a pas de problèmes.
SELECT n56, n57 + n58 + n59 AS ResultByMe FROM import WHERE n57 + n58 + n59 <> n56;

\echo Q3
\echo Nous obtenons la réponse suivante avec LIMIT 10:
SELECT n74, ROUND((n51/(case WHEN n47 <> 0 THEN n47 ELSE 1 END))*100) AS ResultByMe FROM import limit 10;


\echo Q4
\echo Si aucune ligne avec des données s''affiche(mise à part 3 d''entres elles, le professeur ne comprends pas la raison de leurs présence) c''est qu''il n''y a pas de problèmes.
SELECT n74, ROUND((n51/(case WHEN n47 <> 0 THEN n47 ELSE 1 END))*100) AS ResultByMe FROM import GROUP BY n74, n47, n51 HAVING ROUND((n51/(case WHEN n47 <> 0 THEN n47 ELSE 1 END))*100)<> n74;

\echo Q5
\echo Nous obtenons la réponse suivante avec LIMIT 10:
SELECT n76, ROUND(n53/(case WHEN n47 <> 0 THEN n47 ELSE 1 END)*100) AS ResultsByMe FROM import LIMIT 10;

\echo Q6
\echo Nous obtenons la réponse suivante avec LIMIT 10:
SELECT n76, ROUND(e.Eff_adm_prop_fin_procéd_p
/(case WHEN e.Eff_total_cand_accept_prop <> 0 THEN e.Eff_total_cand_accept_prop ELSE 1 END)*100) as resultbyme 
FROM import as i join effectif_admis as e on i.n110 = e.cod_aff_form LIMIT 10;


\echo Q7
\echo Nous obtenons la réponse suivante avec LIMIT 10:
SELECT n81, ROUND(n55/(case WHEN n56 <> 0 THEN n56 ELSE 1 END)*100) AS ResultByMe FROM import GROUP BY n81, n55, n56 LIMIT 10;

\echo Q8
\echo Nous obtenons la réponse suivante avec LIMIT 10:
SELECT i.n81, ROUND(e.Eff_adm_bours_néo_bach/(case WHEN (e.Eff_adm_néo_b_g + e.Eff_adm_néo_b_t +e.Eff_adm_néo_b_pro) <> 0 THEN (e.Eff_adm_néo_b_g + e.Eff_adm_néo_b_t +e.Eff_adm_néo_b_pro) ELSE 1 END)*100) as resultbyme FROM import as i join effectif_admis as e on i.n110 = e.cod_aff_form LIMIT 10;