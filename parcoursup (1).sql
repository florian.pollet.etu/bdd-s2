
\! curl "https://data.enseignementsup-recherche.gouv.fr/api/explore/v2.1/catalog/datasets/fr-esr-parcoursup/exports/csv?lang=fr&timezone=Europe%2FBerlin&use_labels=true&delimiter=%3B" > fr-esr-parcoursup.csv;

\echo start drop
DROP TABLE IF EXISTS Neo_Bacheliers_Effectif CASCADE;
DROP TABLE IF EXISTS Boursiers_Effectif CASCADE;
DROP TABLE IF EXISTS effectif_admis CASCADE;

DROP TABLE IF EXISTS formation CASCADE;

DROP TABLE IF EXISTS etablissement CASCADE;

DROP TABLE IF EXISTS commune CASCADE;

DROP TABLE IF EXISTS import CASCADE;

\echo creation de la table import en cours...

CREATE TABLE import (
    n1 INTEGER DEFAULT NULL,
    n2 CHAR(33) DEFAULT NULL,
    n3 CHAR(8) DEFAULT NULL,
    n4 TEXT DEFAULT NULL,
    n5 CHAR(3) DEFAULT NULL,
    n6 CHAR(24) DEFAULT NULL,
    n7 TEXT DEFAULT NULL,
    n8 TEXT DEFAULT NULL,
    n9 TEXT DEFAULT NULL,
    n10 TEXT DEFAULT NULL,
    n11 TEXT DEFAULT NULL,
    n12 TEXT DEFAULT NULL,
    n13 TEXT DEFAULT NULL,
    n14 TEXT DEFAULT NULL,
    n15 TEXT DEFAULT NULL,
    n16 TEXT DEFAULT NULL,
    n17 TEXT DEFAULT NULL,
    n18 TEXT DEFAULT NULL,
    n19 TEXT DEFAULT NULL,
    n20 TEXT DEFAULT NULL,
    n21 TEXT DEFAULT NULL,
    n22 TEXT DEFAULT NULL,
    n23 TEXT DEFAULT NULL,
    n24 TEXT DEFAULT NULL,
    n25 TEXT DEFAULT NULL,
    n26 TEXT DEFAULT NULL,
    n27 TEXT DEFAULT NULL,
    n28 INTEGER DEFAULT NULL,
    n29 INTEGER DEFAULT NULL,
    n30 INTEGER DEFAULT NULL,
    n31 INTEGER DEFAULT NULL,
    n32 INTEGER DEFAULT NULL,
    n33 INTEGER DEFAULT NULL,
    n34 INTEGER DEFAULT NULL,
    n35 INTEGER DEFAULT NULL,
    n36 INTEGER DEFAULT NULL,
    n37 INTEGER DEFAULT NULL,
    n38 INTEGER DEFAULT NULL,
    n39 INTEGER DEFAULT NULL,
    n40 INTEGER DEFAULT NULL,
    n41 INTEGER DEFAULT NULL,
    n42 INTEGER DEFAULT NULL,
    n43 INTEGER DEFAULT NULL,
    n44 INTEGER DEFAULT NULL,
    n45 INTEGER DEFAULT NULL,
    n46 INTEGER DEFAULT NULL,
    n47 INTEGER DEFAULT NULL,
    n48 INTEGER DEFAULT NULL,
    n49 INTEGER DEFAULT NULL,
    n50 INTEGER DEFAULT NULL,
    n51 FLOAT DEFAULT NULL,
    n52 FLOAT DEFAULT NULL,
    n53 FLOAT DEFAULT NULL,
    n54 FLOAT DEFAULT NULL,
    n55 FLOAT DEFAULT NULL,
    n56 FLOAT DEFAULT NULL,
    n57 FLOAT DEFAULT NULL,
    n58 FLOAT DEFAULT NULL,
    n59 FLOAT DEFAULT NULL,
    n60 INTEGER DEFAULT NULL,
    n61 INTEGER DEFAULT NULL,
    n62 INTEGER DEFAULT NULL,
    n63 INTEGER DEFAULT NULL,
    n64 INTEGER DEFAULT NULL,
    n65 INTEGER DEFAULT NULL,
    n66 FLOAT DEFAULT NULL,
    n67 INTEGER DEFAULT NULL,
    n68 FLOAT DEFAULT NULL,
    n69 INTEGER DEFAULT NULL,
    n70 INTEGER DEFAULT NULL,
    n71 INTEGER DEFAULT NULL,
    n72 INTEGER DEFAULT NULL,
    n73 INTEGER DEFAULT NULL,
    n74 FLOAT DEFAULT NULL,
    n75 FLOAT DEFAULT NULL,
    n76 FLOAT DEFAULT NULL,
    n77 FLOAT DEFAULT NULL,
    n78 FLOAT DEFAULT NULL,
    n79 FLOAT DEFAULT NULL,
    n80 FLOAT DEFAULT NULL,
    n81 FLOAT DEFAULT NULL,
    n82 FLOAT DEFAULT NULL,
    n83 FLOAT DEFAULT NULL,
    n84 FLOAT DEFAULT NULL,
    n85 FLOAT DEFAULT NULL,
    n86 FLOAT DEFAULT NULL,
    n87 FLOAT DEFAULT NULL,
    n88 FLOAT DEFAULT NULL,
    n89 FLOAT DEFAULT NULL,
    n90 FLOAT DEFAULT NULL,
    n91 FLOAT DEFAULT NULL,
    n92 FLOAT DEFAULT NULL,
    n93 FLOAT DEFAULT NULL,
    n94 FLOAT DEFAULT NULL,
    n95 FLOAT DEFAULT NULL,
    n96 FLOAT DEFAULT NULL,
    n97 FLOAT DEFAULT NULL,
    n98 FLOAT DEFAULT NULL,
    n99 FLOAT DEFAULT NULL,
    n100 FLOAT DEFAULT NULL,
    n101 FLOAT DEFAULT NULL,
    n102 CHAR(39) DEFAULT NULL,
    n103 FLOAT DEFAULT NULL,
    n104 CHAR(39) DEFAULT NULL,
    n105 INTEGER DEFAULT NULL,
    n106 CHAR(39) DEFAULT NULL,
    n107 INTEGER DEFAULT NULL,
    n108 CHAR(45) DEFAULT NULL,
    n109 CHAR(19) DEFAULT NULL,
    n110 INTEGER DEFAULT NULL,
    n111 TEXT DEFAULT NULL,
    n112 TEXT DEFAULT NULL,
    n113 FLOAT DEFAULT NULL,
    n114 FLOAT DEFAULT NULL,
    n115 FLOAT DEFAULT NULL,
    n116 FLOAT DEFAULT NULL,
    n117 CHAR(5) DEFAULT NULL,
    n118 CHAR(5) DEFAULT NULL
);

\echo table import valide

\copy import FROM './fr-esr-parcoursup.csv' WITH (FORMAT CSV, DELIMITER ';', HEADER);

\echo creation de la table commune en cours...
CREATE TABLE commune (
    id SERIAL,
    Code_département_de_etabli CHAR(3),
    Département_etablissement CHAR(24),
    Région_etablissement TEXT,
    Académie_etablissement TEXT,
    Commune_etablissement TEXT,
    CONSTRAINT pk_commune PRIMARY KEY (id)
);

\echo table commune valide

\echo insert dans la table commune en cours...
INSERT INTO commune (Code_département_de_etabli, Département_etablissement, Région_etablissement, Académie_etablissement, Commune_etablissement)
SELECT DISTINCT ON (n5, n6, n7, n8, n9) n5, n6, n7, n8, n9
FROM import;
\echo insert dans la table commune valide

\echo creation de la table etablissement en cours...
CREATE TABLE etablissement (
    Code_UAI CHAR(8),
    Statu_etabli_de_filière_forma CHAR(33),
    etablissement TEXT,
    id_region_etablissement INTEGER, 
    CONSTRAINT pk_etablissement PRIMARY KEY (Code_UAI),
    CONSTRAINT fk_etablissement FOREIGN KEY (id_region_etablissement)REFERENCES commune (id) ON UPDATE CASCADE ON DELETE CASCADE
);

\echo table etablissement valide

\echo insert dans la table etablissement en cours...
INSERT INTO etablissement (Code_UAI, Statu_etabli_de_filière_forma, etablissement, id_region_etablissement)
SELECT DISTINCT ON (n3)n3, n2, n4, c.id 
FROM import AS i JOIN commune AS c ON c.Commune_etablissement = i.n9 AND c.Académie_etablissement = i.n8;
\echo insert dans la table etablissement valide

\echo creation de la table formation en cours...
CREATE TABLE formation (
    cod_aff_form INTEGER,
    Filière_de_formation TEXT,
    Sélectivité TEXT,
    Coo_GPS_de_la_formation TEXT,
    Capa_établi_par_formation TEXT,
    Taux_accès FLOAT,
    Code_UAI CHAR(8),
    CONSTRAINT pk_formation PRIMARY KEY (cod_aff_form),
    CONSTRAINT fk_formation FOREIGN KEY (Code_UAI) REFERENCES etablissement (Code_UAI) on update cascade on delete cascade
);
\echo table formation valide

\echo insert dans la table formation en cours...
INSERT INTO formation
SELECT DISTINCT ON (n110) n110, n10, n11, n17, n18, n113, e.Code_UAI
FROM import AS i JOIN etablissement AS e ON e.Code_UAI = i.n3;
\echo insert dans la table formation valide

\echo creation de la table neo_bacheliers_effectif en cours...
CREATE TABLE neo_bacheliers_effectif (
    Eff_total_des_candes_form TEXT,
	Eff_cand_néo_bach_g_ph_p TEXT,
	Eff_cand_néo_bach_t_ph_p TEXT,
	Eff_cand_néo_bach_pro_ph_p TEXT,
	Eff_cand_néo_bach_autre_ph_p INTEGER,
	Eff_cand_néo_bach_autre_ph_c  INTEGER,
	Eff_cand_term_g_prop_adm_etab TEXT, --95
	Eff_cand_term_t_prop_adm_etab TEXT, --97
	Eff_cand_term_pro_prop_adm_etab TEXT, --99
	Eff_cand_autre_prop_adm_etab TEXT, --101
    Eff_cand_n_bach_g_clas_etab INTEGER, --39
    Eff_cand_n_bach_t_clas_etab INTEGER, --41
    Eff_cand_n_bach_pro_clas_etab INTEGER, --43
    Eff_cand_autre_pro_clas_etab INTEGER, --45
	cod_aff_form INTEGER,
	CONSTRAINT pk_neo_bacheliers_effectif PRIMARY KEY (cod_aff_form),
	CONSTRAINT fk_neo_bacheliers_effectif FOREIGN KEY (cod_aff_form) REFERENCES formation (cod_aff_form) on update cascade on delete cascade
);
\echo table neo_bacheliers_effectif valide

\echo insert dans la table neo_bacheliers_effectif en cours...
INSERT INTO neo_bacheliers_effectif
SELECT n20, n23, n25, n27, n29, n30, n95 ,n97 ,n99 ,n101,n39,n41,n43,n45 ,f.cod_aff_form
FROM import AS i JOIN formation AS f ON f.cod_aff_form = i.n110;

\echo creation de la table boursiers_effectif en cours...
CREATE TABLE Boursiers_Effectif (
	Eff_cand_néo_bach_g_phase_p TEXT,	
	Eff_cand_néo_bach_t_phase_p TEXT,
	Eff_cand_néo_bach_pro_phase_p INTEGER,
	Eff_cand_b_tg_prop_adm_etab FLOAT, --n96
	Eff_cand_b_tt_prop_adm_etab FLOAT, --n98
	Eff_cand_b_tp_prop_adm_etab FLOAT, --n100	
	cod_aff_form INTEGER,
	CONSTRAINT pk_Boursiers_Effectif PRIMARY KEY (cod_aff_form),
	CONSTRAINT fk_neo_bacheliers_effectif FOREIGN KEY (cod_aff_form) REFERENCES formation (cod_aff_form) on update cascade on delete cascade
);
\echo table boursiers_effectif valide

\echo insert dans la table boursiers_effectif en cours...
INSERT INTO Boursiers_Effectif
SELECT n24,n26,n28,n96, n98, n100, f.cod_aff_form
FROM import AS i JOIN formation AS f ON f.cod_aff_form = i.n110;
\echo insert dans la table boursiers_effectif valide

\echo creation de la table effectif_admis en cours...
CREATE TABLE effectif_admis (
	Eff_total_cande_admises FLOAT,
	Eff_adm_néo_b_g FLOAT,
	Eff_adm_néo_b_t FLOAT,
	Eff_adm_néo_b_pro FLOAT,
	Eff_adm_néo_b_autre FLOAT,
	Eff_adm_néo_b_pa_info_mt_bac FLOAT, --61
	Eff_adm_néo_b_pa_mt_bac FLOAT, --62
	Eff_adm_néo_b_assezb_mt_bac FLOAT, --63
	Eff_adm_néo_b_b_mention_bac FLOAT, --64
	Eff_adm_néo_b_tb_mt_bac FLOAT, --65
	Eff_adm_néo_b_tb_félicit_bac FLOAT, --66
	Eff_adm_néo_b_g_mention FLOAT, --67
	Eff_adm_néo_b_t_mention FLOAT, --68
	Eff_adm_néo_b_pro_mention FLOAT, --69
	Eff_adm_bours_néo_bach FLOAT,
	Eff_adm_prop_à_ouverture_p FLOAT, --51
	Eff_adm_prop_avant_bac FLOAT, --52
    Eff_adm_prop_fin_procéd_p FLOAT, --53
    Eff_total_cand_accept_prop FLOAT, --47
	cod_aff_form INTEGER,
	CONSTRAINT pk_effectif_admis PRIMARY KEY (cod_aff_form),
	CONSTRAINT fk_effectif FOREIGN KEY (cod_aff_form) REFERENCES formation (cod_aff_form) on update cascade on delete cascade
);
\echo table effectif_admis valide

\echo insert dans la table effectif_admis en cours...
INSERT INTO effectif_admis
SELECT n48,n57,n58,n59,n60,n61,n62,n63,n64,n65,n66,n67,n68,n69,n55,n51,n52,n53,n47,f.cod_aff_form
FROM import AS i JOIN formation AS f ON f.cod_aff_form = i.n110;
\echo insert dans la table effectif_admis valide

select pg_total_relation_size('import') as importsize;

select (pg_total_relation_size('formation') + pg_total_relation_size('etablissement') + pg_total_relation_size('commune') + 
pg_total_relation_size('effectif_admis') + pg_total_relation_size('Boursiers_Effectif') + pg_total_relation_size('neo_bacheliers_effectif') ) as newsize;

